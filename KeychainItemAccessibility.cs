﻿using System;
using System.Collections.Generic;
using CoreFoundation;

namespace KeychainWrapper
{

    public enum KeychainItemAccessibility
    {
        /**
         The data in the keychain item cannot be accessed after a restart until the device has been unlocked once by the user.
         
         After the first unlock, the data remains accessible until the next restart. This is recommended for items that need to be accessed by background applications. Items with this attribute migrate to a new device when using encrypted backups.
        */
        // @available(iOS 4, *)
        AfterFirstUnlock,

        /**
         The data in the keychain item cannot be accessed after a restart until the device has been unlocked once by the user.
     
         After the first unlock, the data remains accessible until the next restart. This is recommended for items that need to be accessed by background applications. Items with this attribute do not migrate to a new device. Thus, after restoring from a backup of a different device, these items will not be present.
         */
        // @available(iOS 4, *)
        AfterFirstUnlockThisDeviceOnly,

        /**
         The data in the keychain item can always be accessed regardless of whether the device is locked.
     
         This is not recommended for application use. Items with this attribute migrate to a new device when using encrypted backups.
         */
        // @available(iOS 4, *)
        Always,

        /**
         The data in the keychain can only be accessed when the device is unlocked. Only available if a passcode is set on the device.
     
         This is recommended for items that only need to be accessible while the application is in the foreground. Items with this attribute never migrate to a new device. After a backup is restored to a new device, these items are missing. No items can be stored in this class on devices without a passcode. Disabling the device passcode causes all items in this class to be deleted.
         */
        // @available(iOS 8, *)
        WhenPasscodeSetThisDeviceOnly,

        /**
         The data in the keychain item can always be accessed regardless of whether the device is locked.
     
         This is not recommended for application use. Items with this attribute do not migrate to a new device. Thus, after restoring from a backup of a different device, these items will not be present.
         */
        // @available(iOS 4, *)
        AlwaysThisDeviceOnly,

        /**
         The data in the keychain item can be accessed only while the device is unlocked by the user.
     
         This is recommended for items that need to be accessible only while the application is in the foreground. Items with this attribute migrate to a new device when using encrypted backups.
     
         This is the default value for keychain items added without explicitly setting an accessibility constant.
         */
        // @available(iOS 4, *)
        WhenUnlocked,

        /**
         The data in the keychain item can be accessed only while the device is unlocked by the user.
     
         This is recommended for items that need to be accessible only while the application is in the foreground. Items with this attribute do not migrate to a new device. Thus, after restoring from a backup of a different device, these items will not be present.
         */
        // @available(iOS 4, *)
        WhenUnlockedThisDeviceOnly,

    }

    public static class KeychainItemAccessibilityHelper
    {

        private static IDictionary<KeychainItemAccessibility, Security.SecAccessible> keychainItemAccessibilityLookup = _privateLookup();

        public static KeychainItemAccessibility? AccessibilityForAttributeValue(Security.SecAccessible keychainAttrValue)
        {
            foreach ((KeychainItemAccessibility key, Security.SecAccessible value) in keychainItemAccessibilityLookup)
            {
                if (value == keychainAttrValue)
                {
                    return key;
                }
            }

            return null;
        }

        public static Security.SecAccessible KeychainAttrValue(KeychainItemAccessibility keychainItemAccessibility) {
            return keychainItemAccessibilityLookup[keychainItemAccessibility];
        }

        private static IDictionary<KeychainItemAccessibility, Security.SecAccessible> _privateLookup() {

            var lookup = new Dictionary<KeychainItemAccessibility, Security.SecAccessible>
            {
                [KeychainItemAccessibility.AfterFirstUnlock] = Security.SecAccessible.AfterFirstUnlock,
                [KeychainItemAccessibility.AfterFirstUnlockThisDeviceOnly] = Security.SecAccessible.AfterFirstUnlockThisDeviceOnly,
                [KeychainItemAccessibility.Always] = Security.SecAccessible.Always,
                [KeychainItemAccessibility.WhenPasscodeSetThisDeviceOnly] = Security.SecAccessible.WhenPasscodeSetThisDeviceOnly,
                [KeychainItemAccessibility.AlwaysThisDeviceOnly] = Security.SecAccessible.AlwaysThisDeviceOnly,
                [KeychainItemAccessibility.WhenUnlocked] = Security.SecAccessible.WhenUnlocked,
                [KeychainItemAccessibility.WhenUnlockedThisDeviceOnly] = Security.SecAccessible.WhenUnlockedThisDeviceOnly
            };

            return lookup;
        }

    }
}
