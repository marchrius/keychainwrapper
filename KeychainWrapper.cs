﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using Security;

namespace KeychainWrapper
{
    /// KeychainWrapper is a class to help make Keychain access in Swift more
    /// straightforward. It is designed to make accessing the Keychain services
    /// more like using NSUserDefaults, which is much more familiar to people.
    public class KeychainWrapper
    {

        [Obsolete("KeychainWrapper.defaultKeychainWrapper is deprecated since version 2.2.1, use KeychainWrapper.standard instead")]
        public static KeychainWrapper DefaultKeychainWrapper = KeychainWrapper.Standard;

        /// Default keychain wrapper access
        public static KeychainWrapper Standard = new KeychainWrapper();

        /// ServiceName is used for the kSecAttrService property to uniquely
        /// identify this keychain accessor. If no service name is specified,
        /// KeychainWrapper will default to using the bundleIdentifier.
        public string ServiceName { get; private set; }

        public string AccessGroup { get; private set; }

        private static string DefaultServiceName => NSBundle.MainBundle.BundleIdentifier ?? "SwiftKeychainWrapper";

        private KeychainWrapper() : this(KeychainWrapper.DefaultServiceName)
        { }

        /// Create a custom instance of KeychainWrapper with a custom Service Name and optional custom access group.
        ///
        /// - parameter serviceName: The ServiceName for this instance. Used to uniquely identify all keys stored using this keychain wrapper instance.
        /// - parameter accessGroup: Optional unique AccessGroup for this instance. Use a matching AccessGroup between applications to allow shared keychain access.
        public KeychainWrapper(string serviceName, string accessGroup = null)
        {
            this.ServiceName = serviceName;
            this.AccessGroup = accessGroup;
        }

        #region Public Methods

        /// Checks if keychain data exists for a specified key.
        ///
        /// - parameter forKey: The key to check for.
        /// - parameter withAccessibility: Optional accessibility to use when retrieving the keychain item.
        /// - returns: True if a value exists for the key. False otherwise.
        public bool HasValue(string key, KeychainItemAccessibility? accessibility = null) {
            if (Data(key, accessibility) != null) {
                return true;
            } else {
                return false;
            }
        }

        public KeychainItemAccessibility? AccessibilityOfKey(string key) {
            var record = SetupKeychainQueryRecord(key);

            // Remove accessibility attribute
            // record.Accessible;

            // Limit search results to one
            // keychainQueryDictionary[SecMatchLimit] = SecMatchLimit.MatchLimitOne;

            // Specify we want SecAttrAccessible returned
            // keychainQueryDictionary[SecReturnAttributes] = true;

            // Search

            var item = SecKeyChain.QueryAsRecord(record, 1, out SecStatusCode status);

            if (status == SecStatusCode.Success && item.Count() > 0)
            {
                var accessibilityAttrValue = item[0].Accessible;
                return KeychainItemAccessibilityHelper.AccessibilityForAttributeValue(accessibilityAttrValue);
            } else
            {
                return null;
            }
        }

        /// Get the keys of all keychain entries matching the current ServiceName and AccessGroup if one is set.
        public ISet<string> AllKeys() {
            var query = new SecRecord(SecKind.GenericPassword)
            {
                Service = ServiceName
            };

            if (AccessGroup != null)
            {
                query.AccessGroup = AccessGroup;
            }

            var results = SecKeyChain.QueryAsRecord(query, SecMatchLimit.MatchLimitAll.ToInt32(), out SecStatusCode status);

            var keys = new HashSet<string>();
            if (status != SecStatusCode.Success)
            {
                return keys;
            }

            foreach(var result in results)
            {
                if (result.Account != null)
                {
                    var key = result.Account;
                    keys.Add(key);
                }
            }
 
            return keys;
        }

        #endregion

        #region Public Getters

        public NSNumber? Number(string key, KeychainItemAccessibility? accessibility = null)
        {
            var data = Object(key, accessibility);

            if (data == null) return null;

            return data as INSCoding as NSNumber;
        }

        public ulong? UnsignedLong(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.UnsignedLongValue;
        }

        public long? Long(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.LongValue;
        }

        public int? Integer(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.Int32Value;
        }

        public uint? UnsignedInteger(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.UInt32Value;
        }

        public nuint? NUnsignedInteger(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.NUIntValue;
        }

        public nint? NInteger(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.NIntValue;
        }

        public float? Float(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.FloatValue;
        }

        public nfloat? NFloat(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.NFloatValue;
        }


        public double? Double(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.DoubleValue;
        }

        public bool? Bool(string key, KeychainItemAccessibility? accessibility = null)
        {
            return Number(key, accessibility)?.BoolValue;
        }

        /// Returns a string value for a specified key.
        ///
        /// - parameter forKey: The key to lookup data for.
        /// - parameter withAccessibility: Optional accessibility to use when retrieving the keychain item.
        /// - returns: The String associated with the key if it exists. If no data exists, or the data found cannot be encoded as a string, returns nil.
        public string String(string key, KeychainItemAccessibility? accessibility = null) {
            var data = Data(key, accessibility);

            if (data == null) return null;

            return NSString.FromData(data, NSStringEncoding.UTF8);
        }

        /// Returns an object that conforms to NSCoding for a specified key.
        ///
        /// - parameter forKey: The key to lookup data for.
        /// - parameter withAccessibility: Optional accessibility to use when retrieving the keychain item.
        /// - returns: The decoded object associated with the key if it exists. If no data exists, or the data found cannot be decoded, returns nil.
        public NSCoding Object(string key, KeychainItemAccessibility? accessibility = null) {
            var data = Data(key, accessibility);
            if (data == null)
            {
                return null;
            }

            return NSKeyedUnarchiver.UnarchiveObject(data) as NSCoding;
        }

        /// Returns a Data object for a specified key.
        ///
        /// - parameter forKey: The key to lookup data for.
        /// - parameter withAccessibility: Optional accessibility to use when retrieving the keychain item.
        /// - returns: The Data object associated with the key if it exists. If no data exists, returns nil.
        public NSData? Data(string key, KeychainItemAccessibility? accessibility = null) {
            var query = SetupKeychainQueryRecord(key, accessibility);

            // Limit search results to one
            // keychainQueryDictionary[SecMatchLimit] = kSecMatchLimitOne

            // Specify we want Data/CFData returned
            // keychainQueryDictionary[SecReturnData] = kCFBooleanTrue

            // Search
            var result = SecKeyChain.QueryAsData(query, false, 1, out SecStatusCode status);

            return (status == SecStatusCode.Success && result.Count() > 0) ? result[0] : null;
        }

        /// Returns a persistent data reference object for a specified key.
        ///
        /// - parameter forKey: The key to lookup data for.
        /// - parameter withAccessibility: Optional accessibility to use when retrieving the keychain item.
        /// - returns: The persistent data reference object associated with the key if it exists. If no data exists, returns nil.
        public NSData? DataRef(string key, KeychainItemAccessibility? accessibility = null) {
            var query = SetupKeychainQueryRecord(key, accessibility);

            // Limit search results to one
            // keychainQueryDictionary[SecMatchLimit] = kSecMatchLimitOne

            // Specify we want persistent Data/CFData reference returned
            // keychainQueryDictionary[SecReturnPersistentRef] = kCFBooleanTrue

            query.PersistentReference = true;

            // Search
            var result = SecKeyChain.QueryAsData(query, true, 1, out SecStatusCode status);

            return (status == SecStatusCode.Success && result.Count() > 0) ? result[0] : null;
        }

        #endregion

        #region Public Setters

        public bool Set(NSNumber value, string key, KeychainItemAccessibility? accessibility = null) {
            return Set((INSCoding) value, key, accessibility);
        }

        public bool Set(nint value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromNInt(value), key, accessibility);
        }

        public bool Set(int value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromInt64(value), key, accessibility);
        }

        public bool Set(uint value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromUInt64(value), key, accessibility);
        }

        public bool Set(nuint value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromNUInt(value), key, accessibility);
        }

        public bool Set(long value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromInt64(value), key, accessibility);
        }

        public bool Set(ulong value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromUInt64(value), key, accessibility);
        }

        public bool Set(nfloat value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromNFloat(value), key, accessibility);
        }

        public bool Set(float value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromFloat(value), key, accessibility);
        }

        public bool Set(double value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromDouble(value), key, accessibility);
        }

        public bool Set(bool value, string key, KeychainItemAccessibility? accessibility = null)
        {
            return Set(NSNumber.FromBoolean(value), key, accessibility);
        }

        /// Save a String value to the keychain associated with a specified key. If a String value already exists for the given key, the string will be overwritten with the new value.
        ///
        /// - parameter value: The String value to save.
        /// - parameter forKey: The key to save the String under.
        /// - parameter withAccessibility: Optional accessibility to use when setting the keychain item.
        /// - returns: True if the save was successful, false otherwise.
        public bool Set(string value, string key, KeychainItemAccessibility? accessibility = null) {
            var data = NSData.FromString(value, NSStringEncoding.UTF8);
            if (data != null) {
                return Set(data, key, accessibility);
            } else {
                return false;
            }
        }

        /// Save an NSCoding compliant object to the keychain associated with a specified key. If an object already exists for the given key, the object will be overwritten with the new value.
        ///
        /// - parameter value: The NSCoding compliant object to save.
        /// - parameter forKey: The key to save the object under.
        /// - parameter withAccessibility: Optional accessibility to use when setting the keychain item.
        /// - returns: True if the save was successful, false otherwise.
        public bool Set(INSCoding value, string key, KeychainItemAccessibility? accessibility = null) {
            var data = NSKeyedArchiver.ArchivedDataWithRootObject((NSCoding) value);

            return Set(data, key, accessibility);
        }

        /// Save a Data object to the keychain associated with a specified key. If data already exists for the given key, the data will be overwritten with the new value.
        ///
        /// - parameter value: The Data object to save.
        /// - parameter forKey: The key to save the object under.
        /// - parameter withAccessibility: Optional accessibility to use when setting the keychain item.
        /// - returns: True if the save was successful, false otherwise.
        public bool Set(NSData value, string key, KeychainItemAccessibility? accessibility = null) {
            var query = SetupKeychainQueryRecord(key, accessibility);


            query.ValueData = value;
        
            if (accessibility.HasValue) {
                query.Accessible = KeychainItemAccessibilityHelper.KeychainAttrValue(accessibility.Value);
            } else {
                // Assign default protection - Protect the keychain entry so it's only valid when the device is unlocked
                query.Accessible = KeychainItemAccessibilityHelper.KeychainAttrValue(KeychainItemAccessibility.WhenUnlocked);
            }

            var status = SecKeyChain.Add(query);


            if (status == SecStatusCode.Success) {
                return true;
            } else if (status == SecStatusCode.DuplicateItem) {
                return Update(value, key, accessibility);
            } else {
                return false;
            }
        }

        [Obsolete("remove is deprecated since version 2.2.1, use removeObject instead")]
        public bool Remove(string key, KeychainItemAccessibility? accessibility = null) {
            return RemoveObject(key, accessibility);
        }

        /// Remove an object associated with a specified key. If re-using a key but with a different accessibility, first remove the previous key value using removeObjectForKey(:withAccessibility) using the same accessibilty it was saved with.
        ///
        /// - parameter forKey: The key value to remove data for.
        /// - parameter withAccessibility: Optional accessibility level to use when looking up the keychain item.
        /// - returns: True if successful, false otherwise.
        public bool RemoveObject(string key, KeychainItemAccessibility? accessibility = null) {
            var query = SetupKeychainQueryRecord(key, accessibility);

            // Delete
            var status = SecKeyChain.Remove(query);

            if (status == SecStatusCode.Success) {
                return true;
            } else {
                return false;
            }
        }

        /// Remove all keychain data added through KeychainWrapper. This will only delete items matching the currnt ServiceName and AccessGroup if one is set.
        public bool RemoveAllKeys() {
            // Setup dictionary to access keychain and specify we are using a generic password (rather than a certificate, internet password, etc)
            var record = new SecRecord(SecKind.GenericPassword)
            {
                // Uniquely identify this keychain accessor
                Service = ServiceName
            };
        
            // Set the keychain access group if defined
            if (AccessGroup != null) {
                record.AccessGroup = AccessGroup;
            }

            var status = SecKeyChain.Remove(record);
        
            if (status == SecStatusCode.Success) {
                return true;
            } else {
                return false;
            }
        }

        /// Remove all keychain data, including data not added through keychain wrapper.
        ///
        /// - Warning: This may remove custom keychain entries you did not add via SwiftKeychainWrapper.
        ///
        public void WipeKeychain()
        {
            DeleteKeychainSecKind(SecKind.GenericPassword); // Generic password items
            DeleteKeychainSecKind(SecKind.InternetPassword); // Internet password items
            DeleteKeychainSecKind(SecKind.Certificate); // Certificate items
            DeleteKeychainSecKind(SecKind.Key); // Cryptographic key items
            DeleteKeychainSecKind(SecKind.Identity); // Identity items
        }

        #endregion

        #region Private Methods

        /// Remove all items for a given Keychain Item Class
        ///
        ///
        private bool DeleteKeychainSecKind(SecKind kind) {
            var query = new SecRecord(kind);

            //            [SecClass: secClass]
            var status = SecKeyChain.Remove(query);
        
            if (status == SecStatusCode.Success) {
                return true;
            } else {
                return false;
            }
        }

        /// Update existing data associated with a specified key name. The existing data will be overwritten by the new data.
        private bool Update(NSData value, string key, KeychainItemAccessibility? accessibility = null) {
            var record = SetupKeychainQueryRecord(key, accessibility);

            var updateRecord = new SecRecord(SecKind.GenericPassword);
            updateRecord.ValueData = value;

            // on update, only set accessibility if passed in
            if (accessibility.HasValue) {
                record.Accessible = KeychainItemAccessibilityHelper.KeychainAttrValue(accessibility.Value);
            }

            // Update
            var status = SecKeyChain.Update(record, updateRecord);

            if (status == SecStatusCode.Success) {
                return true;
            } else {
                return false;
            }
        }

        /// Setup the keychain query dictionary used to access the keychain on iOS for a specified key name. Takes into account the Service Name and Access Group if one is set.
        ///
        /// - parameter forKey: The key this query is for
        /// - parameter withAccessibility: Optional accessibility to use when setting the keychain item. If none is provided, will default to .WhenUnlocked
        /// - returns: A dictionary with all the needed properties setup to access the keychain on iOS
        private SecRecord SetupKeychainQueryRecord(string key, KeychainItemAccessibility? accessibility = null) {
            // Setup default access as generic password (rather than a certificate, internet password, etc)
            var record = new SecRecord(SecKind.GenericPassword)
            {
                // Uniquely identify this keychain accessor
                Service = ServiceName
            };

            // Uniquely identify the account who will be accessing the keychain
            var encodedIdentifier = NSData.FromString(key, NSStringEncoding.UTF8);


            record.Generic = encodedIdentifier;
            record.Account = key;
            record.Label = key;

            // Only set accessibiilty if its passed in, we don't want to default it here in case the user didn't want it set
            if (accessibility.HasValue)
            {
                record.Accessible = KeychainItemAccessibilityHelper.KeychainAttrValue(accessibility.Value);
            }

            // Set the keychain access group if defined
            if (AccessGroup != null)
            {
                record.AccessGroup = AccessGroup;
            }

            return record;
        }
    }

    #endregion
}